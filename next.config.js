module.exports = {
  target: 'serverless',
  basePath: process.env.NODE_ENV === 'production' ? '/next-static' : '',
  assetPrefix: process.env.NODE_ENV === 'production' ? '/next-static/' : '',
  webpack: function (config) {
    config.module.rules.push({
      test: /\.md$/,
      use: 'raw-loader',
    })
    return config
  },
}
